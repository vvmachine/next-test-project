import { create } from "zustand";


export const useContactsStore = create<UseContactsTypes>()((set) => ({
  contacts: [],
  addNewContact: (contact) => {
    set((state) => ({ contacts: [...state.contacts, contact] }));
  },
  deleteContact: (contactId) => {
    set((state) => ({
      contacts: state.contacts.filter((contact) => contact.id !== contactId),
    }));
  },

  setStoragedData: (contactsArr) => set(() => ({ contacts: [...contactsArr] })),
}));
