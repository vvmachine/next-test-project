import { z } from "zod";

const phoneRegEx =
  /((\+38)?\(?\d{3}\)?[\s\.-]?(\d{7}|\d{3}[\s\.-]\d{2}[\s\.-]\d{2}|\d{3}-\d{4}))/;

export const contactSchema = z.object({
  name: z.string().min(1, { message: "Name is required" }),
  email: z.string().email({ message: "Wrong email" }),
  phone: z.string().regex(phoneRegEx, { message: "Wrong phone" }),
});
