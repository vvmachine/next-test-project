import axios from "axios";

const basicOptions = {
  method: "GET",
  headers: {
    "X-RapidAPI-Key": "3a80eeaaecmsha050f3aeb33b2ebp164d4bjsn921306083ca8",
    "X-RapidAPI-Host": "api-football-v1.p.rapidapi.com",
  },
};

export const getTeamByNameSearch = async (query: string) => {
  const options = {
    ...basicOptions,
    url: "https://api-football-v1.p.rapidapi.com/v3/teams",
    params: { search: query },
  };

  try {
    const response = await axios.request(options);

    return response.data;
  } catch (error) {
    return error;
  }
};

export const getTeamById = async (id: number) => {
  const options = {
    ...basicOptions,
    url: "https://api-football-v1.p.rapidapi.com/v3/teams",
    params: { id },
  };

  try {
    const response = await axios.request(options);

    return response.data;
  } catch (error) {
    return error;
  }
};


export const getSquadByTeamId = async (id: number) => {
  const options = {
    ...basicOptions,
    url: "https://api-football-v1.p.rapidapi.com/v3/players/squads",
    params: { team: id },
  };

  try {
    const response = await axios.request(options);

    return response.data;
  } catch (error) {
    return error;
  }
};
