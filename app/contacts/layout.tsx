import type { Metadata } from "next";

export const metadata: Metadata = {
  title: "VVMachine | Contacts",
  description: "Test next app by VVMachine",
};

export default function ContactsLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <>{children}</>;
}
