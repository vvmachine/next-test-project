import AddContactForm from "@/components/contacts/AddContactForm";
import ContactsList from "@/components/contacts/ContactsList";

export default function Contacts() {
  return (
    <div className="flex justify-center">
      <div className="mx-5 border border-spacing-1 w-full max-w-screen-md">
        <AddContactForm />
        <ContactsList />
      </div>
    </div>
  );
}
