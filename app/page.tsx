export default function Home() {
  return (
    <>
      <h1 className="text-center text-5xl font-bold">
        This is test VVMachine NextJS App
      </h1>
      <p className="text-2xl">
        The application includes the following technologies:{" "}
      </p>
      <ul className="list-disc ml-5 my-2 text-xl">
        <li>NextJS 13</li>
        <li>Tailwind CSS (incl. Resposive styles)</li>
        <li>React Hook form</li>
        <li>Zod validator</li>
        <li>React Hooks (useState, useEffect)</li>
        <li>Zustand State Manager</li>
        <li>Axios</li>
        <li>React Query</li>
      </ul>

      <p className="text-2xl mb-2">All technologies divided on 2 chapters.</p>
      <p className="text-2xl mb-2">
        Chapter 1 (Contacts) includes React Hook Form, Zod, React hooks and
        Zustand.
      </p>
      <p className="text-2xl mb-2">
        Chapter 2 (Football) using RapidAPI (API-Football) with using React
        Query and Axios for queries
      </p>
    </>
  );
}
