import SearchInput from "@/components/football/SearchInput/SearchInput";
import TeamList from "@/components/football/TeamsList/TeamLists";

export default function Football({
  searchParams,
}: {
  searchParams: { [key: string]: string | undefined };
}) {
  const teamSearchQuery =
    typeof searchParams.team === "string" ? searchParams.team : "";

  return (
    <div>
      <div className="flex flex-col items-center">
        <SearchInput />
        <span className="mt-2">
          {teamSearchQuery
            ? `Results for ${teamSearchQuery}:`
            : "Input team and press Search button"}
        </span>

        {teamSearchQuery !== "" && <div className="flex"><TeamList searchQuery={teamSearchQuery} /></div>}
      </div>
    </div>
  );
}
