import type { Metadata } from "next";

export const generateMetadata = async ({
  params: { id },
}: TeamInfoParams): Promise<Metadata> => {
  return {
    title: `VVMachine | Team details`,
  };
};

export { default } from "./TeamInfo";
