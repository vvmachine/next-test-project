import SquadDetails from "@/components/football/SquadDetails/SquadDetails";
import TeamDetails from "@/components/football/TeamDetails/TeamDetails";

export default function TeamInfo({ params: { id } }: TeamInfoParams) {
  const numberTeamId = Number(id);


  return (
    <div>
      {isNaN(numberTeamId) ? (
        <div className="text-center text-2xl text-red-400">Wrong teamId: {id}</div>
      ) : (
        <div className="flex flex-col justify-center">
          <TeamDetails teamId={numberTeamId} />
          <SquadDetails teamId={numberTeamId} />
        </div>
      )}
    </div>
  );
}
