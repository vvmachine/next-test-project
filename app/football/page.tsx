import type { Metadata } from "next";

export const metadata: Metadata = {
    title: "VVMachine | Football",
    description: "Test next app by VVMachine",
  };

export {default} from './Football'