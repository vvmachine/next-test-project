import Header from "@/components/header/header";
import Providers from "@/lib/providers";
import "@/styles/globals.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "VVMachine | Home",
  description: "Test next app by VVMachine",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en" data-theme="light">
      <body suppressHydrationWarning={true} className={inter.className}>
        <Header />
        <main className="m-auto max-w-screen-xl p-10">
          <Providers>{children}</Providers>
        </main>
      </body>
    </html>
  );
}
