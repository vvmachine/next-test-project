interface ITeam {
  code: string;
  country: string;
  founded: number;
  id: number;
  logo: string;
  name: string;
}

interface IVenue {
  address: string;
  capacity: number;
  city: string;
  id: number;
  image: string;
  name: string;
}

interface ITeamData {
  team: ITeam;
  venue: IVenue;
}

interface IRespApi {
  errors: any;
  get: string;
  paging: {
    current: number;
    total: number;
  };
  response: ITeamData[];
  results: number;
}

interface TeamInfoParams {
  params: {
    id: string;
  };
}

interface IPlayerInfo {
  id: number;
  age: number;
  name: string;
  number: number;
  photo: string;
  position: string;
}
