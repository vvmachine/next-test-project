interface ContactsFormTypes {
  name: string;
  email: string;
  phone: string;
}

interface ContactsInStore extends ContactsFormTypes {
  id: string;
}

interface UseContactsTypes {
  contacts: ContactsInStore[];
  addNewContact: (contact: ContactsInStore) => void;
  deleteContact: (contactId: string) => void;
  setStoragedData: (contactsArr: ContactsInStore[]) => void;
}
