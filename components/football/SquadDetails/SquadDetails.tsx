"use client";

import { useQuery } from "@tanstack/react-query";
import { getSquadByTeamId } from "@/services/api-football";

import PlayerCard from "../PlayerCard/PlayerCard";

export default function SquadDetails({ teamId }: { teamId: number }) {
  const { data, isLoading, isError } = useQuery(["teamSquad", teamId], () =>
    getSquadByTeamId(teamId)
  );

  const playersData: IPlayerInfo[] =
    data && data.results > 0 ? data.response[0].players : [];

  const gkPlayers = playersData.filter(
    (player) => player.position === "Goalkeeper"
  );
  const defPlayers = playersData.filter(
    (player) => player.position === "Defender"
  );
  const midPlayers = playersData.filter(
    (player) => player.position === "Midfielder"
  );
  const attPlayers = playersData.filter(
    (player) => player.position === "Attacker"
  );

  if (isLoading) {
    return <div className="text-center">Loading teams...</div>;
  }

  if (isError && data.results === 0) {
    return (
      <div className="text-center text-2xl text-red-400">
        Whoops!!! Something going wrong...
      </div>
    );
  }

  if (!isLoading && !isError && playersData) {
    return (
      <div className="mt-5">
        <h3 className=" text-3xl font-semibold text-center">Squad details</h3>

        {data.results === 0 ? (
          <div className="text-center text-2xl text-red-400">
            No players in team
          </div>
        ) : (
          <>
            <h4 className=" text-2xl font-semibold my-5">Goalkeepers</h4>
            <div className="w-full flex justify-center md:justify-start">
              <ul className="flex flex-wrap gap-5 justify-center md:justify-start">
                {gkPlayers.map((player) => (
                  <li key={player.id}>
                    <PlayerCard playerData={player} />
                  </li>
                ))}
              </ul>
            </div>

            <h4 className=" text-2xl font-semibold my-5">Defenders</h4>
            <div className="w-full flex justify-center md:justify-start">
              <ul className="flex flex-wrap gap-5 justify-center md:justify-start">
                {defPlayers.map((player) => (
                  <li key={player.id}>
                    <PlayerCard playerData={player} />
                  </li>
                ))}
              </ul>
            </div>

            <h4 className=" text-2xl font-semibold my-5">Midfielders</h4>
            <div className="w-full flex justify-center md:justify-start">
              <ul className="flex flex-wrap gap-5 justify-center md:justify-start">
                {midPlayers.map((player) => (
                  <li key={player.id}>
                    <PlayerCard playerData={player} />
                  </li>
                ))}
              </ul>
            </div>

            <h4 className=" text-2xl font-semibold my-5">Attakers</h4>
            <div className="w-full flex justify-center md:justify-start">
              <ul className="flex flex-wrap gap-5 justify-center md:justify-start">
                {attPlayers.map((player) => (
                  <li key={player.id}>
                    <PlayerCard playerData={player} />
                  </li>
                ))}
              </ul>
            </div>
          </>
        )}
      </div>
    );
  }
}
