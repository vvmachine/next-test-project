import Image from "next/image";

export default function PlayerCard({
  playerData,
}: {
  playerData: IPlayerInfo;
}) {
  const { photo, name, age, number } = playerData;

  return (
    <div className="border rounded p-3 flex flex-col w-[200px]">
      <div className="flex justify-center">
        <div className="w-36 h-36 my-4 w">
          <Image
            src={photo}
            alt={name}
            width={0}
            height={0}
            sizes="100vw"
            className="w-full h-full object-contain"
          />
        </div>
      </div>

      <div>
        <span className="font-semibold">Name: </span>
        <span>{name}</span>
      </div>

      <div>
        <span className="font-semibold">Age: </span>
        <span>{age}</span>
      </div>

      <div>
        <span className="font-semibold">Number: </span>
        <span>{number}</span>
      </div>
    </div>
  );
}
