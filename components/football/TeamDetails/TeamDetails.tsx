"use client";

import { useQuery } from "@tanstack/react-query";
import { getTeamById } from "@/services/api-football";

import Image from "next/image";

import imgPlaceholder from '@/public/placeholder.jpg'

export default function TeamDetails({ teamId }: { teamId: number }) {
  const { data, isLoading, isError } = useQuery(["teamInfo", teamId], () =>
    getTeamById(teamId)
  );

  const teamData: ITeamData = data && data.response[0];
  

  if (isLoading) {
    return <div className="text-center">Loading teams...</div>;
  }

  if (isError || data.results === 0) {
    return (
      <div className="text-center text-2xl text-red-400">
        Whoops!!! Something going wrong...
      </div>
    );
  }

  if (!isLoading && !isError && teamData) {
    return (
      <div>
        <div className="flex flex-col md:flex-row md:justify-center md:gap-x-5">
          <div className="flex flex-col items-center md:mx-10 md:items-start">
            <h3 className="text-2xl font-bold">Team information</h3>
            <div className="flex flex-col-reverse w-[260px] md:h-full md:justify-between">
              <ul className="text-lg">
                <li>
                  <span className="font-semibold">Name: </span>
                  <span>{teamData.team.name}</span>
                </li>

                <li>
                  <span className="font-semibold">Short name: </span>
                  <span>{teamData.team.code || teamData.team.name.slice(0,3).toUpperCase()}</span>
                </li>

                <li>
                  <span className="font-semibold">Country: </span>
                  <span>{teamData.team.country}</span>
                </li>

                <li>
                  <span className="font-semibold">Founded: </span>
                  <span>{teamData.team.founded}</span>
                </li>
              </ul>

              <div className="w-full flex justify-center md:justify-start">
                <div className="w-36 h-36 my-4 w">
                  <Image
                    src={teamData.team.logo}
                    alt={teamData.team.name}
                    width={0}
                    height={0}
                    sizes="100vw"
                    className="w-full h-full object-contain"
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="flex flex-col items-center my-4 md:my-0 md:mx-10">
            <h3 className="text-2xl font-bold">Stadium information</h3>
            <div className="flex flex-col-reverse w-[260px] md:w-full">
              <ul className="text-lg">
                <li>
                  <span className="font-semibold">Name: </span>
                  <span>{teamData.venue.name || 'no data'}</span>
                </li>

                <li>
                  <span className="font-semibold">City: </span>
                  <span>{teamData.venue.city  || 'no data'}</span>
                </li>

                <li>
                  <span className="font-semibold">Address: </span>
                  <span>{teamData.venue.address || 'no data'}</span>
                </li>

                <li>
                  <span className="font-semibold">Capacity: </span>
                  <span>{teamData.venue.capacity || 'no data'}</span>
                </li>
              </ul>

              <div className="w-[260px] h-[195px] my-4">
                <Image
                  src={teamData.venue.image || imgPlaceholder}
                  alt={teamData.venue.name || 'stadium'}
                  width={0}
                  height={0}
                  sizes="100vw"
                  className="w-full h-full object-contain"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
