"use client";

import { useQuery } from "@tanstack/react-query";
import { getTeamByNameSearch } from "@/services/api-football";
import Image from "next/image";
import Link from "next/link";

export default function TeamList({ searchQuery }: { searchQuery: string }) {
  const { data, isLoading, isError } = useQuery(["teams", searchQuery], () =>
    getTeamByNameSearch(searchQuery)
  );

  const limitedData: ITeamData[] = data && data.response.slice(0, 20);

  if (isLoading) {
    return <div>Loading teams...</div>;
  }

  if (isError || data.results === 0) {
    return <div>Whoops!!! Something going wrong...</div>;
  }

  if (!isLoading && !isError && limitedData) {
    return (
      <ul className="flex flex-col gap-4 mt-4 w-52 sm:flex-row sm:flex-wrap sm:justify-start sm:w-[432px] md:w-[656px] lg:w-[880px] xl:w-[1104px]">
        {limitedData.map((team) => (
          <li key={team.team.id} className=" w-52">
            <Link
              className="p-2 flex flex-col items-center border-2 rounded hover:bg-gray-300 active:bg-gray-400"
              href={`/football/team/${team.team.id}`}
            >
              <div className="w-36 h-36">
                <Image
                  src={team.team.logo}
                  alt={team.team.name}
                  width={0}
                  height={0}
                  sizes="100vw"
                  className="w-full h-full object-contain"
                />
              </div>
              <div className="text-lg font-semibold">{team.team.name}</div>
              <div>{team.team.country}</div>
            </Link>
          </li>
        ))}
      </ul>
    );
  }
}
