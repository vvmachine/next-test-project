"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";

import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";

export default function SearchInput() {
  const [query, setQuery] = useState("");
  const [queryErrorLength, setQueryErrorLength] = useState(false);
  const router = useRouter();

  const searchInputHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(e.target.value);
  };

  const searchBtnHandler = () => {
    if (query.length < 3) {
      setQueryErrorLength(true);
      return;
    }
    setQueryErrorLength(false);
    router.push(`/football?team=${query.toLowerCase()}`);
  };

  return (
    <div className="flex flex-col w-60 items-center">
      <label className="flex flex-col mb-2">
        <span>Input team to search:</span>
        <Input
          type="text"
          placeholder="ex. Arsenal/Real/Chelsea"
          value={query}
          onChange={searchInputHandler}
        />
      </label>
      <Button className="w-20" variant="accent" onClick={searchBtnHandler}>
        Search
      </Button>

      {queryErrorLength && (
        <span className=" mt-2 text-red-400 font-semibold">
          The name of the team must be longer then 3 characters
        </span>
      )}
    </div>
  );
}
