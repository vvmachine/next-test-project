"use client";

import { useEffect, useState } from "react";
import { useContactsStore } from "@/store/contacts";

import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";

export default function ContactsList() {
  const { contacts, setStoragedData, deleteContact } = useContactsStore();
  const [filter, setFilter] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setIsLoading(true);

    const localStorageData = window.localStorage.getItem("nextContacts");

    if (!localStorageData) {
      setIsLoading(false);
      return;
    }

    const parsedContacts = JSON.parse(localStorageData);

    setStoragedData(parsedContacts);
    setIsLoading(false);
  }, [setStoragedData]);

  const deleteHandler = (contactId: string) => {
    const localStorageContacts = window.localStorage.getItem("nextContacts");

    if (localStorageContacts) {
      const pasredContacts = JSON.parse(localStorageContacts);

      const newContactsArray = pasredContacts.filter(
        (contact: ContactsInStore) => contact.id !== contactId
      );

      window.localStorage.setItem(
        "nextContacts",
        JSON.stringify(newContactsArray)
      );
    }

    deleteContact(contactId);
  };

  const changeFilterHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFilter(e.target.value);
  };

  return (
    <div className="my-5 px-5">
      <h3 className="text-xl font-bold mb-2">Contacts</h3>

      {isLoading ? (
        <div>Loading...</div>
      ) : contacts && contacts.length === 0 ? (
        <div>No added contacts</div>
      ) : (
        <div>
          <label className="mb-4 flex flex-col mt-2 sm:flex-row sm:items-center">
            <span className="w-28">Filter (by name): </span>
            <Input
              label="Filter"
              type="text"
              id="filter"
              placeholder="ex. Joe"
              value={filter}
              onChange={changeFilterHandler}
            />
          </label>

          <h3 className="text-xl font-bold mb-2">Contacts List</h3>

          <ul>
            {contacts
              .filter((contact: ContactsInStore) =>
                contact.name
                  .toLocaleLowerCase()
                  .includes(filter.toLocaleLowerCase())
              )
              .map((contact: ContactsInStore) => {
                const { id, name, email, phone } = contact;

                return (
                  <li
                    key={id}
                    className="flex flex-col mb-4 md:flex-row md:items-center md:justify-between"
                  >
                    <span className="text-left md:w-4/12">{name}</span>
                    <span className="text-left md:w-4/12">{email}</span>
                    <span className="text-left md:w-3/12">{phone}</span>
                    <span className="flex justify-start md:w-2/12 md:justify-end">
                      <Button
                        className="w-20"
                        variant="danger"
                        onClick={() => deleteHandler(id)}
                      >
                        Delete
                      </Button>
                    </span>
                  </li>
                );
              })}
          </ul>
        </div>
      )}
    </div>
  );
}
