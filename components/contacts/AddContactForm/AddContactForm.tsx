"use client";
import { useEffect } from "react";

import { SubmitHandler, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { contactSchema } from "@/validation/contactValidator";

import { useContactsStore } from "@/store/contacts";

import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";

export default function AddContactForm() {
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting, isSubmitSuccessful },
    reset,
  } = useForm<ContactsFormTypes>({
    defaultValues: {
      name: "",
      email: "",
      phone: "",
    },
    resolver: zodResolver(contactSchema),
  });

  const { contacts, addNewContact } = useContactsStore();

  const addContactToLS = (contact: ContactsInStore) => {
    const localStorageContacts = window.localStorage.getItem("nextContacts");

    if (localStorageContacts) {
      const pasredContacts = JSON.parse(localStorageContacts);
      const newContactsArray = [...pasredContacts, contact];

      window.localStorage.setItem(
        "nextContacts",
        JSON.stringify(newContactsArray)
      );

      return;
    }

    window.localStorage.setItem("nextContacts", JSON.stringify([contact]));
  };

  const onSubmitHandler: SubmitHandler<ContactsFormTypes> = (data) => {
    const nameIsInContacts = contacts.some(
      (contact: ContactsFormTypes) => contact.name === data.name
    );

    if (nameIsInContacts) {
      alert(`Contact wint name ${data.name} is already in contacts`);
      return;
    }

    const emailIsInContacts = contacts.some(
      (contact: ContactsFormTypes) => contact.email === data.email
    );

    if (emailIsInContacts) {
      alert(`Contact wint email ${data.email} is already in contacts`);
      return;
    }

    const numberIsInContacts = contacts.some(
      (contact: ContactsFormTypes) => contact.email === data.email
    );

    if (numberIsInContacts) {
      alert(`Contact wint phonenumber ${data.phone} is already in contacts`);
      return;
    }

    const newContact = {
      id: new Date().toString(),
      ...data,
    };

    addNewContact(newContact);

    addContactToLS(newContact);

    reset({ ...data });
  };

  useEffect(() => {
    if (isSubmitSuccessful) {
      reset({ name: "", email: "", phone: "" });
    }
  }, [isSubmitSuccessful, reset]);

  return (
    <div className="mx-5 mt-5">
      <h3 className="text-xl font-bold mb-2">Add contact form</h3>
      <form onSubmit={handleSubmit(onSubmitHandler)} className="flex flex-col">
        <label className="flex flex-col mt-2 sm:flex-row sm:items-center">
          <span className="w-14">Name: </span>
          <Input
            label="Name"
            type="text"
            id="name"
            placeholder="ex. John Doe"
            disabled={isSubmitting}
            {...register("name")}
          />

          {errors.name?.message && (
            <div className="text-red-500">{errors.name?.message}</div>
          )}
        </label>

        <label className="flex flex-col mt-2 sm:flex-row sm:items-center">
          <span className="w-14">Email: </span>
          <Input
            label="Email"
            type="email"
            id="email"
            disabled={isSubmitting}
            placeholder="ex. mail@mail.com"
            {...register("email")}
          />
          {errors.email?.message && (
            <div className="text-red-500">{errors.email?.message}</div>
          )}
        </label>

        <label className="flex flex-col mt-2 sm:flex-row sm:items-center">
          <span className="w-14">Phone: </span>
          <Input
            label="Phonenumber"
            type="tel"
            id="phone"
            placeholder="ex. (+380(XX)XXX-XX-XX)"
            disabled={isSubmitting}
            {...register("phone")}
          />
          {errors.phone?.message && (
            <div className="text-red-500">{errors.phone?.message}</div>
          )}
        </label>

        <Button
          variant="positive"
          disabled={isSubmitting}
          className="w-32 mt-4"
        >
          Add contact
        </Button>
      </form>
    </div>
  );
}
