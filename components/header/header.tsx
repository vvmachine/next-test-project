"use client";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";

import logo from "@/public/logo.png";

export default function Header() {
  const pathname = usePathname();

  const activeLinkStyle = "underline underline-offset-4 text-orange-400";

  return (
    <header className="flex h-14 bg-blue-900 text-white justify-between items-center px-2 md:h-20 md:px-4 lg:px-8">
      <div className="h-14 md:h-20 ">
        <Link href="/">
          <Image
            priority
            src={logo}
            alt="logo"
            className="w-auto h-full object-cover"
          />
        </Link>
      </div>

      <nav>
        <ul className="flex gap-x-5 text-md md:text-xl">
          <li className={pathname === "/" ? activeLinkStyle : ""}>
            <Link href="/">Home</Link>
          </li>
          <li className={pathname === "/contacts" ? activeLinkStyle : ""}>
            <Link href="/contacts">Contacts</Link>
          </li>
          <li className={pathname.includes("football") ? activeLinkStyle : ""}>
            <Link href="/football">Football</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}
