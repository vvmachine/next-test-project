/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
    images: {
        remotePatterns: [
            {
                protocol: 'https',
                hostname: 'media-4.api-sports.io',
                port: '',
                pathname: '/football/**'
            }
        ]
    }
}

module.exports = nextConfig
